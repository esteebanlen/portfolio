const skillsData = [
  {
    title: "FRONT-END",
    skills: [
      { name: "React.JS", value: 70 },
      { name: "React Native", value: 70 },
      { name: "JavaScript", value: 60 },
    ],
  },
  {
    title: "BACK-END",
    skills: [
      { name: "PostgreSQL", value: 60 },
      { name: "MySQL", value: 60 },
    ],
  },
  {
    title: "UI/UX",
    skills: [
      { name: "Photoshop", value: 90 },
      { name: "Illustrator", value: 70 },
    ],
  },
];

export default skillsData;
