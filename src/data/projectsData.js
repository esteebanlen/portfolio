import torontoImg   from "../assets/img/BarberiaToronto.png";
import portfolioImg from "../assets/img/PortfolioWeb.png";

const projectsData=[
    { 
        id:1, 
        title:"Portfolio Web", 
        img:[portfolioImg], 
        desc:"Descrpción"
    },    
    { 
        id:2, 
        title:"Toronto Web", 
        img:[torontoImg],
        desc:"Descrpción"
    },    
    { 
        id:3, 
        title:"Toronto App", 
        img:[torontoImg], 
        desc:"Descrpción"
    },
];

export default projectsData;