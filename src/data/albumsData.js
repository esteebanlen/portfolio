import foto1_1 from "../assets/img/album1/foto1.jpg";
import foto1_2 from "../assets/img/album1/foto2.jpg";
import foto1_3 from "../assets/img/album1/foto3.jpg";
import foto1_4 from "../assets/img/album1/foto4.jpg";
import foto1_5 from "../assets/img/album1/foto5.jpg";

import foto2_1 from "../assets/img/album2/foto1.jpg";
import foto2_2 from "../assets/img/album2/foto2.jpg";
import foto2_3 from "../assets/img/album2/foto3.jpg";
import foto2_4 from "../assets/img/album2/foto4.jpg";
import foto2_5 from "../assets/img/album2/foto5.jpg";

import foto3_1 from "../assets/img/album3/foto1.jpg";
import foto3_2 from "../assets/img/album3/foto2.jpg";
import foto3_3 from "../assets/img/album3/foto3.jpg";
import foto3_4 from "../assets/img/album3/foto4.jpg";
import foto3_5 from "../assets/img/album3/foto5.jpg";

const albumsData=[
    { 
        id:1, 
        title:"La Cumbrecita", 
        subtitle:"Viaje hecho a la Cumbrecita, Córdoba", 
        img:[foto1_1,foto1_2,foto1_3,foto1_4,foto1_5], 
        desc:"Descrpción"
    },    
    { 
        id:2, 
        title:"Temaikèn", 
        subtitle:"Viaje a Bio Parque Temaikèn", 
        img:[foto2_1,foto2_2,foto2_3,foto2_4,foto2_5],
        desc:"Descrpción"
    },    
    { 
        id:3, 
        title:"Santa Fe", 
        subtitle:"Fotos de Ciudad de Santa Fe en 2020", 
        img:[foto3_1,foto3_2,foto3_3,foto3_4,foto3_5], 
        desc:"Descrpción"
    },
];

export default albumsData;