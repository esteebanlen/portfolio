import foto1_1 from "../img/album1/foto1.jpg";
import foto1_2 from "../img/album1/foto2.jpg";
import foto1_3 from "../img/album1/foto3.jpg";
import foto1_4 from "../img/album1/foto4.jpg";
import foto1_5 from "../img/album1/foto5.jpg";

import foto2_1 from "../img/album2/foto1.jpg";
import foto2_2 from "../img/album2/foto2.jpg";
import foto2_3 from "../img/album2/foto3.jpg";
import foto2_4 from "../img/album2/foto4.jpg";

import foto3_1 from "../img/album3/foto1.jpg";
import foto3_2 from "../img/album3/foto2.jpg";
import foto3_3 from "../img/album3/foto3.jpg";
import foto3_4 from "../img/album3/foto4.jpg";
import foto3_5 from "../img/album3/foto5.jpg";

import foto4_1 from "../img/album4/foto1.jpg";
import foto4_2 from "../img/album4/foto2.jpg";
import foto4_3 from "../img/album4/foto3.jpg";

import foto5_1 from "../img/album5/foto1.jpg";
import foto5_2 from "../img/album5/foto2.jpg";
import foto5_3 from "../img/album5/foto3.jpg";
import foto5_4 from "../img/album5/foto4.jpg";
import foto5_5 from "../img/album5/foto5.jpg";

import foto6_1 from "../img/album6/foto1.jpg";
import foto6_2 from "../img/album6/foto2.jpg";
import foto6_3 from "../img/album6/foto3.jpg";
import foto6_4 from "../img/album6/foto4.jpg";

const dataList=[
    {
        id:1, title:"Proyecto 1", subtitle:"Este es el subtítulo del proyecto", 
        img:[foto1_1,foto1_2,foto1_3,foto1_4,foto1_5], 
        desc:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae feugiat sem. Vestibulum sit amet ex dapibus, mollis magna at, pulvinar sapien. Morbi vel urna vitae ipsum cursus vehicula eu ac magna. Nulla quis est sagittis, posuere tellus vel, luctus magna. Aliquam vel fermentum erat. Nunc tristique a tortor sed luctus. Nulla a nibh et risus condimentum elementum. Aenean condimentum vel ante in maximus. Suspendisse potenti. Aenean mattis vulputate ipsum ac mollis. Maecenas sagittis, dolor vitae consequat consectetur, arcu lacus faucibus justo, quis ornare metus arcu malesuada nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum blandit bibendum mattis. Nulla euismod at purus a condimentum. In quis erat ultricies, luctus sapien sit amet, porta lectus. Etiam aliquam luctus purus, nec semper ipsum consectetur sit amet. Sed imperdiet facilisis fringilla. Maecenas ac ipsum finibus, consequat est in, dictum mauris. Etiam at efficitur ligula. Phasellus efficitur lacus non augue euismod pretium. Aliquam aliquam elementum arcu, in finibus tellus interdum at. Phasellus ut commodo enim. Fusce mattis sem a vehicula pellentesque. Mauris a magna egestas, euismod lacus eget, imperdiet nibh. Pellentesque at scelerisque turpis, suscipit pretium velit. Quisque commodo sapien id commodo egestas. Etiam justo neque, interdum vitae lacus ac, blandit sollicitudin libero. Integer convallis est felis, et consectetur sapien aliquet ac. "
    },
    {
        id:2, title:"Proyecto 2", subtitle:"Este es el subtítulo del proyecto", 
        img:[foto3_1,foto3_2,foto3_3,foto3_4,foto3_5], 
        desc:""
    },
    {
        id:3, title:"Proyecto 3", subtitle:"Este es el subtítulo del proyecto", 
        img:[foto4_1,foto4_2,foto4_3], 
        desc:""
    },
    {
        id:4, title:"Proyecto 4", subtitle:"Este es el subtítulo del proyecto", 
        img:[foto5_1,foto5_2,foto5_3,foto5_4,foto5_5], 
        desc:""
    },
    {
        id:5, title:"Proyecto 5", subtitle:"Este es el subtítulo del proyecto", 
        img:[foto6_1,foto6_2,foto6_3,foto6_4], 
        desc:""
    },
    {
        id:6, title:"Proyecto 6", subtitle:"Este es el subtítulo del proyecto demasiado largo para ver que pasa con el Card component", 
        img:[foto2_1,foto2_2,foto2_3,foto2_4],
        desc:""
    },
];


export default dataList;